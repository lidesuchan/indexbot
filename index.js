/*
* Definições
*/

var Discord = require('discord.js');
var client = new Discord.Client();
var prefix = ''
var token = ''

/*
** Evento message
*/
client.on("message", async (message) => {
	if(message.author.bot) return;
	if(message.channel.type == 'dm') return;
	if(!message.content.startsWith(prefix)) return;
	
	let args = message.content.slice(prefix.length).trim().split(/ +/g)
	let comando = args.shift().toLowerCase();
	
	if(comando == 'ping'){
		message.channel.send(`Tenho ${parseInt(client.ping)} ms de Ping`)
	}
	if(comando == 'clear'){
		if(message.member.hasPermission(["ADMINISTRATOR"])){ // se tiver a permissão ADMINISTRADOR(A)
			if(args[0]){ // se tiver args[0] ( args[0] é isso aqui : clear <args[0]>
				message.channel.bulkDelete(args[0]) // limpa as mensagens
				message.channel.send(`${args[0]} mensagem(ns) foram deletadas`).then(value => { // manda uma mensagem
					value.delete(5000) // invés de 5000 colocar quantidade de segundos em milissegundos, exemplo : 5 segundos = 5000
				})
			} else {
				message.channel.send("Diga a quantidade de mensagens para eu deletar")
			}
		} else {
			message.channel.send("Você não tem permissão para isso")
			}
	}
})
/*
** Evento Ready
*/

client.on("ready", async ({}) => {
	console.log("iniciado") // loga algo
	client.user.setActivity('algo', {type: 'PLAYING'}) // coloca a presence "algo" sem as aspas
	/*
	* PLAYING = JOGANDO
	* LISTENING = OUVINDO
	* WATCHING = ASSISTINDO
	* STREAMING = TRANSMITINDO ( precisa de url, exemplo : client.user.setActivity('algo', {type: 'STREAMING', url: 'https://twitch.tv/algo'});
	*/
	return; 
})

client.login(token);